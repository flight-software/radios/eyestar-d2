#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>

#include "librpc.h"
#include "mission_ops.h"
#include "eyestar_d2.h"

void print_raw(void* data_in, size_t total_size, size_t per_line);

int main(void)
{
    uint64_t timeout_us = 5*1000*1000; // 5 seconds
    while(1) {
        printf("For CHECK_ALIVE, press 1\nFor GET_RX_COUNT, press 2\nFor GET_TX_COUNT, press 3\nFor PAYLOAD_TX, press 4\nFor PAYLOAD_RX, press 5\nFor CHECK_SOH, press 6\nFor CLEAR_TX_QUEUE, press 7\nFor GET_LAST_TX, press 8\nFor BEGIN_NOMINAL?, press 9\nFor QUERY_TX_STATUS, press 10\nFor SCIENCE_TX, press 11\n===========================\n> ");
        int choice = 0;
        scanf("%d", &choice);
        switch(choice) {
            case 1:;
            {
                if(rpc_trigger_action(RSD_RPC_SOCKET, RSD_OP_CHECK_ALIVE, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 2:;
            {
                uint32_t count = 0;
                if(rpc_recv_uint32(RSD_RPC_SOCKET, RSD_OP_GET_RX_COUNT, &count, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("RX count == %"PRIu32"\n", count);
                }
            }
            break;
            case 3:;
            {
                uint32_t count = 0;
                if(rpc_recv_uint32(RSD_RPC_SOCKET, RSD_OP_GET_TX_COUNT, &count, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("TX count == %"PRIu32"\n", count);
                }
            }
            break;
            case 4:;
            {
                char disk_filename[1024] = {0};
                printf("Enter full disk path: ");
                scanf("%s", disk_filename);
                char tx_filename[1024] = {0};
                printf("Enter TX filename: ");
                scanf("%s", tx_filename);
                size_t disk_size = strlen(disk_filename);
                size_t tx_size = strlen(tx_filename);
                char* result = calloc(disk_size + tx_size + 2, sizeof(char));
                memcpy(result, disk_filename, disk_size);
                memcpy(result + disk_size + 1, tx_filename, tx_size);
                if(rpc_send_buf(RSD_RPC_SOCKET, RSD_OP_PAYLOAD_TX, result, disk_size + tx_size + 2, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
                free(result);
            }
            break;
            case 5:;
            {
                uint8_t data[8192];
                size_t s = sizeof(data);
                if(rpc_recv_buf_varlen(RSD_RPC_SOCKET, RSD_OP_PAYLOAD_RX, data, &s, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    print_raw(data, s, 8);
                }
            }
            break;
            case 6:;
            {
                eyestar_d2_soh_fs soh = {0};
                if(rpc_recv_buf(RSD_RPC_SOCKET, RSD_OP_CHECK_SOH, &soh, sizeof(soh), timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    print_raw(&soh, sizeof(soh), 4);
                }
            }
            break;
            case 7:;
            {
                if(rpc_trigger_action(RSD_RPC_SOCKET, RSD_OP_CLEAR_TX_QUEUE, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 8:;
            {
                uint64_t time;
                if(rpc_recv_buf(RSD_RPC_SOCKET, RSD_OP_GET_LAST_TX_S, &time, sizeof(time), timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("Last TX time (sec): %"PRIu64"\n", time);
                }
            }
            break;
            case 9:;
            {
                uint64_t mode;
                if(rpc_recv_uint64(RSD_MISSION_RPC_SOCKET, RSD_OP_BEGIN_NOMINAL, &mode, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    if(mode == 0x0) {
                        printf("Mode: RADIO_STAY_HOLD");
                    } else if(mode == 0x1) {
                        printf("Mode: RADIO_BEGIN_NOMINAL");
                    } else {
                        printf("Mode did not match, %d", (int)mode);
                    }
                }
            }
            break;
            case 10:;
            {
                uint64_t mode;
                if(rpc_recv_buf(RSD_MISSION_RPC_SOCKET, RSD_OP_QUERY_TX_STATUS, &mode, sizeof(mode), timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    if(mode == 0x2) {
                        printf("Mode: RADIO_NOT_DONE");
                    } else if(mode == 0x3) {
                        printf("Mode: RADIO_TX_DONE");
                    } else {
                        printf("Mode did not match, %d", (int)mode);
                    }
                }
            }
            break;
            case 11:;
            {
                if(rpc_trigger_action(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            default:;
        }
    }
}

void print_raw(void* data_in, size_t total_size, size_t per_line)
{
    uint8_t* data = data_in;
    for(size_t i = 0; i < total_size; i += per_line) {
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            printf("%02x ", data[i + j]);
        }
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            uint8_t d = data[i + j];
            if(32 <= d && d <= 127) {
                printf("%c ", (char)d);
            } else {
                printf(". ");
            }
        }
        printf("\n");
    }
}

