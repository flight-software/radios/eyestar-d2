#include <stdint.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include "libdebug.h"
#include "libtty.h"
#include "librpc.h"
#include "powerboard.h"
#include "eyestar_d2.h"

#define CHECK_ALIVE_WAIT_SEC (20) // Check alive every 20 seconds

static uint16_t crc16(const uint8_t* ptr, int count);
static int eyestar_d2_write_data(const uint8_t* data, size_t len);
static int eyestar_d2_read_data(uint8_t* data, size_t* len);

static rpc_server_fs rpc_server;
static int eyestar_d2_serial_fd;
static uint64_t last_tx_s;
static uint8_t rx_data[8190];

static time_t last_check_alive = 0;

pthread_mutex_t serial_lock = PTHREAD_MUTEX_INITIALIZER;

// yeah I know we are using "int" for count but it works for now
// Dillon Hammond 10/09/2018
static uint16_t crc16(const uint8_t* ptr, int count)
{
    int  crc;
    char i;
    crc = 0x0;
    while (--count >= 0)
    {
        crc = crc ^ (int)*ptr++ << 8; // I'm sorry
        i = 8;
        do
        {
            if (crc & 0x8000)
                crc = crc << 1 ^ 0x1021;
            else
                crc = crc << 1;
        } while (--i);
    }
    return (uint16_t)crc;

}

static int eyestar_d2_check_alive_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED CHECK ALIVE RPC");
    (void)in; (void)in_len; (void)out; (void)out_len; (void)param;
    return eyestar_d2_check_alive();
}

static int eyestar_d2_get_rx_count_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED RX COUNT RPC");
    (void)in; (void)in_len; (void)param;
    if(*out_len < sizeof(uint32_t)) {
        P_ERR_STR("Can't return rx count in < 4 bytes");
        return EXIT_FAILURE;
    }
    *out_len = sizeof(uint32_t);
    return eyestar_d2_get_rx_count((uint32_t*)out);
}

static int eyestar_d2_get_tx_count_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED TX COUNT RPC");
    (void)in; (void)in_len; (void)param;
    if(*out_len < sizeof(uint32_t)) {
        P_ERR_STR("Can't return tx count in < 4 bytes");
        return EXIT_FAILURE;
    }
    *out_len = sizeof(uint32_t);
    return eyestar_d2_get_tx_count((uint32_t*)out);
}

int eyestar_d2_payload_tx_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED PAYLOAD TX RPC");
    (void)out; (void)out_len; (void)param;
    const char* disk_filename = (char*)in;
    P_DEBUG("Got disk_filename: %s", disk_filename);
    size_t tx_filename_begin = 0;
    while(disk_filename[tx_filename_begin]) tx_filename_begin += 1;
    tx_filename_begin += 1;
    if(tx_filename_begin >= in_len) {
        P_ERR_STR("No TX Filename given");
    }
    const char* tx_filename = disk_filename + tx_filename_begin;
    P_DEBUG("Got tx_filename: %s", tx_filename);
    int fd = open(disk_filename, O_RDONLY);
    if(fd < 0) {
        P_ERR("open %s failed, errno: %d (%s)", disk_filename, errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }
    struct stat s = {0};
    if(fstat(fd, &s)) {
        P_ERR("fstat %s failed, errno: %d (%s)", disk_filename, errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }
    uint8_t* disk_data = mmap(NULL, (size_t)s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    close(fd);
    if(disk_data == MAP_FAILED) {
        P_ERR("Failed to mmap %s, errno: %d (%s)", disk_filename, errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }
    int r = eyestar_d2_payload_tx(tx_filename, disk_data, (size_t)s.st_size);
    munmap(disk_data, (size_t)s.st_size);
    if(r == EXIT_SUCCESS && strncmp("hm-d", tx_filename, 4) != 0) {
        P_INFO("Deleting file: %s", disk_filename);
        if(unlink(disk_filename)) {
            P_ERR("Failed to unlink %s, errno: %d (%s)", disk_filename, errno, strerror(errno));
            errno = 0;
        }
    }
    return r;
}

static int eyestar_d2_payload_rx_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED PAYLOAD RX RPC");
    (void)in; (void)in_len; (void)param;
    size_t rx_data_len;
    if(eyestar_d2_payload_rx(rx_data, &rx_data_len)) {
        P_ERR_STR("Failed to get data from radio");
        return EXIT_FAILURE;
    }
    if(rx_data_len > *out_len) {
        P_ERR("%zu too small for %zu bytes from radio, copying what we can", *out_len, rx_data_len);
        rx_data_len = *out_len;
    } else {
        *out_len = rx_data_len;
    }
    memcpy(out, rx_data, rx_data_len);
    return EXIT_SUCCESS;
}

static int eyestar_d2_read_soh_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED PAYLOAD SOH RPC");
    (void)in; (void)in_len; (void)param;
    if(*out_len < sizeof(eyestar_d2_soh_fs)) {
        P_ERR_STR("Out len was smaller than soh packet");
        return EXIT_FAILURE;
    }
    return eyestar_d2_read_soh((eyestar_d2_soh_fs*)out);
}

static int eyestar_d2_delete_tx_queue_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param)
{
    P_DEBUG_STR("CALLED DELETE TX QUEUE RPC");
    (void)in; (void)in_len; (void)out; (void)out_len; (void)param;
    return eyestar_d2_delete_tx_queue();
}

int eyestar_d2_init(char *path){
    startSerial(&eyestar_d2_serial_fd, path, B38400, 1, 10);
    if(rpc_server_init(&rpc_server, RSD_RPC_SOCKET, RPC_SERVER_MT)) {
        P_ERR_STR("RPC FAILED");
        exit(EXIT_FAILURE);
    }
    rpc_server_add_elem(&rpc_server, RSD_OP_CHECK_ALIVE, eyestar_d2_check_alive_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_GET_RX_COUNT, eyestar_d2_get_rx_count_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_GET_TX_COUNT, eyestar_d2_get_tx_count_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_PAYLOAD_TX, eyestar_d2_payload_tx_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_PAYLOAD_RX, eyestar_d2_payload_rx_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_CHECK_SOH, eyestar_d2_read_soh_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_CLEAR_TX_QUEUE, eyestar_d2_delete_tx_queue_rpc, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_GET_LAST_TX_S, rpc_rpc_get_uint64, &last_tx_s, NULL);
    return EXIT_SUCCESS;
}

int eyestar_d2_loop(void)
{
    eyestar_d2_check_alive();
    return EXIT_SUCCESS;
}

int eyestar_d2_close(void)
{
    tcflush(eyestar_d2_serial_fd, TCIOFLUSH);
    close(eyestar_d2_serial_fd);

    if(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, HOTSWAP_J11, 5*1000*1000)){
        P_ERR_STR("Can't disable EyeStar-D2 hotspot");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int eyestar_d2_check_alive(void)
{
    time_t now = time(NULL);
    if(now - last_check_alive < CHECK_ALIVE_WAIT_SEC) return EXIT_SUCCESS;
    last_check_alive = now;
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        if(eyestar_d2_write_data((uint8_t*)"GUGETALV", sizeof("GUGETALV") - 1)) {
            P_ERR_STR("Failed write GUGETALV");
            r = EXIT_FAILURE;
            goto rsd_check_alive_ret;
        }
        uint8_t ack[3] = {0xFF, 0xFF, 0xFF};
        size_t s = sizeof(ack);
        if(eyestar_d2_read_data(ack, &s)) {
            P_ERR_STR("Failed to check alive");
            r = EXIT_FAILURE;
            goto rsd_check_alive_ret;
        }
        P_DEBUG("ALIVE? RESPONSE: (%zu) 0x%02x%02x%02x", s, ack[0], ack[1], ack[2]);
        if(!(ack[0] == 0x47 && ack[1] == 0x55 && ack[2] == 0x06 && s == sizeof(ack))) {
            P_ERR_STR("GUGETALV did NOT ACK");
            r = EXIT_FAILURE;
            goto rsd_check_alive_ret;
        }
    }
rsd_check_alive_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

int eyestar_d2_get_rx_count(uint32_t* count)
{
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        if(eyestar_d2_write_data((uint8_t*)"GUGETUFC", sizeof("GUGETUFC") - 1)) {
            P_ERR_STR("Failed to write GUGETUFC");
            r = EXIT_FAILURE;
            goto ed2_get_rx_count_ret;
        }
        uint8_t countArr[6];
        memset(countArr, 0xFF, 6);
        size_t s = sizeof(countArr);
        if(eyestar_d2_read_data(countArr, &s)) {
            P_ERR_STR("Failed to read get rx count");
            r = EXIT_FAILURE;
            goto ed2_get_rx_count_ret;
        }
        P_DEBUG("GUGETUFC: (%zu) 0x%x%x, %x_%x_%x_%x", s, countArr[0], countArr[1], countArr[2], countArr[3], countArr[4], countArr[5]);
        if(!(countArr[0] == 0x47 && countArr[1] == 0x55 && s == sizeof(countArr))) {
            P_ERR_STR("GUGETUFC did NOT ACK");
            r = EXIT_FAILURE;
            goto ed2_get_rx_count_ret;
        }
        *count = be32toh(*((uint32_t*)(countArr + 2)));
    }
ed2_get_rx_count_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

int eyestar_d2_get_tx_count(uint32_t* count)
{
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        if(eyestar_d2_write_data((uint8_t*)"GUGETDFC", sizeof("GUGETDFC") - 1)) {
            P_ERR_STR("Failed to write GUGETDFC");
            r = EXIT_FAILURE;
            goto ed2_get_tx_count_ret;
        }
        uint8_t countArr[6];
        memset(countArr, 0xFF, 6);
        size_t s = sizeof(countArr);
        if(eyestar_d2_read_data(countArr, &s)) {
            P_ERR_STR("Failed to check tx count");
            r = EXIT_FAILURE;
            goto ed2_get_tx_count_ret;
        }
        P_DEBUG("GUGETDFC: (%zu) 0x%x%x, %x_%x_%x_%x", s, countArr[0], countArr[1], countArr[2], countArr[3], countArr[4], countArr[5]);
        if(!(countArr[0] == 0x47 && countArr[1] == 0x55 && s == sizeof(countArr))) {
            P_ERR_STR("GUGETDFC did NOT ACK");
            r = EXIT_FAILURE;
            goto ed2_get_tx_count_ret;
        }
        *count = be32toh(*((uint32_t*)(countArr + 2)));
    }
ed2_get_tx_count_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

int eyestar_d2_payload_tx(const char *filename, const uint8_t *data, size_t len)
{
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        int i = 0;
    try_again:
        if(eyestar_d2_write_data((uint8_t*)"GUPUT_DF", sizeof("GUPUT_DF") - 1)) {
            P_ERR_STR("GUPUT_DF failed to write");
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }
        uint8_t ack[3] = {0xFF, 0xFF, 0xFF};
        size_t s = sizeof(ack);
        if(eyestar_d2_read_data(ack, &s)) {
            if(i == 0) {
                i += 1;
                goto try_again;
            }
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }
        P_DEBUG("GUPUT_DF: (%zu) 0x%x%x%x", s, ack[0], ack[1], ack[2]);
        if(!(ack[0] == 0x47 && ack[1] == 0x55 && ack[2] == 0x06 && s == sizeof(ack))) {
            P_ERR_STR("GUPUT_DF did NOT ACK");
            if(i == 0) {
                i += 1;
                goto try_again;
            }
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }
        size_t bytes_no_payload;
        {
            int ret = snprintf(NULL, 0, "GU%03d%06d%s", strlen(filename), len, filename) + 1; // snprintf doesn't count NULL byte here
            if(ret < 0) {
                P_ERR("snprintf gave us %d, errno: %d (%s)", ret, errno, strerror(errno));
                errno = 0;
                r = EXIT_FAILURE;
                goto ed2_payload_tx_ret;
            }
            bytes_no_payload = (size_t)ret;
        }
        uint8_t* packet = calloc(bytes_no_payload, sizeof(uint8_t));

        snprintf((char*)packet, bytes_no_payload, "GU%03d%06d%s", strlen(filename), len, filename);
        
        size_t payload_start = bytes_no_payload - 1; // index of the NULL byte snprintf injected
        
        size_t total_bytes_no_crc = bytes_no_payload + len - 1;
        
        packet = realloc(packet, total_bytes_no_crc);
        
        memcpy(packet + payload_start, data, len); // will overwrite starting at NULL byte
        
        uint16_t crc = crc16(packet, (int)total_bytes_no_crc);
        size_t total_bytes = total_bytes_no_crc + 2;
        
        packet = realloc(packet, total_bytes + 2);
        packet[total_bytes - 2] = (uint8_t)(crc >> 8);
        packet[total_bytes - 1] = (uint8_t)(crc & 0x0000ffff);

        // time_t tx_time = ((8 * total_bytes) / 38400) + 2;
        // tv = (struct timeval){.tv_sec = tx_time, .tv_usec = 0};
        // Approx. seconds to TX file
    /*
#if(DEBUG) // This shit is pretty useful but make sure it doesnt do anything in flight
        for(size_t ii = 0; ii < (size_t)total_bytes; ii += 4) {
            P_DEBUG("%02x%02x%02x%02x", packet[ii], packet[ii+1], packet[ii+2], packet[ii+3]);
        }
        P_DEBUG("ignore last: %d bytes", 4 - (total_bytes % 4));
#endif
    */
        if(eyestar_d2_write_data(packet, (size_t)total_bytes)) {
            P_ERR_STR("Failed to TX file to radio");
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }

        memset(ack, 0xFF, sizeof(ack));
        s = sizeof(ack);
        if(eyestar_d2_read_data(ack, &s)) {
            P_ERR_STR("didn't ack");
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }
        P_DEBUG("File TX: (%zu) 0x%x%x%x", s, ack[0], ack[1], ack[2]);
        if(!(ack[0] == 0x47 && ack[1] == 0x55 && ack[2] == 0x06 && s == sizeof(ack))) {
            P_ERR_STR("File TX did NOT ACK");
            r = EXIT_FAILURE;
            goto ed2_payload_tx_ret;
        }
        last_tx_s = (uint64_t)time(NULL);
        P_DEBUG("We TX'd a file to radio! (%s)", filename);
    }
ed2_payload_tx_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

uint64_t eyestar_d2_get_last_tx_s(void)
{
    return last_tx_s;
}

int eyestar_d2_payload_rx(uint8_t *data, size_t *len)
{
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        if(eyestar_d2_write_data((uint8_t*)"GUGET_UF", sizeof("GUGET_UF") - 1)) { // LOCKS
            *len = 0;
            r = EXIT_FAILURE;
            goto ed2_payload_rx_ret;
        }
        
        char payload[999999];
        memset(payload, 0xFF, 2);
        size_t init_read = sizeof(payload);
        if(eyestar_d2_read_data((uint8_t*)payload, &init_read)) {
            P_ERR_STR("Couldn't read back from radio");
            eyestar_d2_preserve_download();
            r = EXIT_FAILURE;
            goto ed2_payload_rx_ret;
        }
        P_DEBUG("GUGET_UF: (%zu) 0x%x%x", init_read, payload[0], payload[1]);
        if(!(payload[0] == 0x47 && payload[1] == 0x55)) {
            P_ERR_STR("GUGET_UF did NOT ACK");
            eyestar_d2_preserve_download();
            r = EXIT_FAILURE;
            goto ed2_payload_rx_ret;
        }
        P_DEBUG("%02x%02x %02x%02x%02x %02x%02x%02x%02x%02x%02x %02x%02x%02x%02x%02x", payload[0],payload[1],payload[2],payload[3],payload[4],payload[5],payload[6],payload[7],payload[8],payload[9],payload[10],payload[11],payload[12],payload[13], payload[14],payload[15]);
        char file_name_size_str[4] = {payload[2], payload[3], payload[4], 0x0};
        int file_name_size = atoi(file_name_size_str);
        char file_size_str[7] = {payload[5], payload[6], payload[7], payload[8], payload[9], payload[10], 0x0};
        size_t file_size = (size_t)atoi(file_size_str);
        size_t header_size = 2 + 3 + 6 + (size_t)file_name_size;
        size_t missing_header = header_size - init_read;
        size_t header_read_fix = missing_header;
        if(header_read_fix > 0) {
            if(eyestar_d2_read_data((uint8_t*)payload + init_read, &header_read_fix)) {
                P_ERR_STR("Couldn't read header fix");
                eyestar_d2_preserve_download();
                r = EXIT_FAILURE;
                goto ed2_payload_rx_ret;
            }
        }
        P_DEBUG("%d == %d + %d", (int)header_size, (int)init_read, (int)header_read_fix);
        size_t num_bytes_read = 0;
        while(num_bytes_read < file_size) {
            size_t si = sizeof(payload) - init_read - num_bytes_read;
            if(eyestar_d2_read_data((uint8_t*)payload + header_size + num_bytes_read, &si)) {
                P_ERR_STR("Failed while trying to read in file contents");
                break;
            }
            if(si == 0) break;
            si = num_bytes_read + si <= file_size ? si : file_size - num_bytes_read;
            num_bytes_read += si;
            P_DEBUG("READ %zu/%d bytes", num_bytes_read, file_size);
        }
        P_DEBUG("total read: %d", (int)(header_size + num_bytes_read));
        memcpy(data, payload + header_size, num_bytes_read);
        *len = num_bytes_read;
        
        eyestar_d2_delete_download();
    }
ed2_payload_rx_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

int eyestar_d2_delete_tx_queue(void)
{
    pthread_mutex_lock(&serial_lock);
    int r = EXIT_SUCCESS;
    {
        if(eyestar_d2_write_data((uint8_t*)"GUDLTQDF", sizeof("GUDLTQDF") - 1)) {
            P_ERR_STR("Failed to write GUDLTQDF");
            r = EXIT_FAILURE;
            goto ed2_delete_tx_queue_ret;
        }
        uint8_t resp[6];
        memset(resp, 0xFF, 6);
        size_t s = sizeof(resp);
        eyestar_d2_read_data(resp, &s);
        if(!(resp[0] == 0x47 && resp[1] == 0x55 && s == 6)) {
            P_ERR_STR("GUDLTQDF failed to ACK");
            r = EXIT_FAILURE;
            goto ed2_delete_tx_queue_ret;
        }
    }
ed2_delete_tx_queue_ret:
    pthread_mutex_unlock(&serial_lock);
    return r;
}

static uint8_t be8toh(uint8_t x) { return x; }

// For some reason this is at best returning 16 bytes-ish
// Dillon Hammond 10/09/2018
int eyestar_d2_read_soh(eyestar_d2_soh_fs *soh)
{
    pthread_mutex_lock(&serial_lock);
    uint8_t resp[37];
    size_t resp_len = sizeof(resp);
    {
        if(eyestar_d2_write_data((uint8_t*)"GUGETSOH", sizeof("GUGETSOH") - 1)) {
            P_ERR_STR("Failed to write GUGETSOH");
            pthread_mutex_unlock(&serial_lock);
            return EXIT_FAILURE;
        }
        eyestar_d2_read_data(resp, &resp_len);
    }
    pthread_mutex_unlock(&serial_lock);
    if(resp[0] == 0x47 && resp[1] == 0x55 && resp_len == sizeof(resp)) {
        uint8_t* resp_ptr = resp + 2;
        /*
         * soh->epoch_reset = be32toh(*(uint32_t*)resp);
         * resp += sizeof(soh->epoch_reset);
         */
#define READ_DATA(dest___, len___) \
            dest___ = be ## len___ ## toh (*(uint ## len___ ## _t*)resp_ptr);\
            resp_ptr += sizeof(dest___);
        READ_DATA(soh->epoch_reset, 32);
        READ_DATA(soh->cur_time_s, 32);
        READ_DATA(soh->rssi, 8);
        READ_DATA(soh->connection_status, 8);
        READ_DATA(soh->gateway, 8);
        READ_DATA(soh->last_connect_time_s, 32);
        READ_DATA(soh->last_attempt_time_s, 32);
        READ_DATA(soh->call_attempts, 32);
        READ_DATA(soh->successful_connects, 32);
        READ_DATA(soh->avg_conn_dur_s, 32);
        READ_DATA(soh->std_dev_conn_dur_s, 32);
#undef READ_DATA
        return EXIT_SUCCESS;
    }
    P_ERR_STR("GUGETSOH did NOT ACK");
    return EXIT_FAILURE;
}

static int eyestar_d2_write_data(const uint8_t* data, size_t len)
{
    tcflush(eyestar_d2_serial_fd, TCIOFLUSH);
    usleep(100 * 1000); //1ms
    if(write(eyestar_d2_serial_fd, data, len) == (ssize_t)len) { // Mutex unlocked by read
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

static int eyestar_d2_read_data(uint8_t* data, size_t* len)
{
    ssize_t r = read(eyestar_d2_serial_fd, data, *len);
    if(r < 0) {
        P_ERR("read failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    P_INFO("read %zu bytes", (size_t)r);
    *len = (size_t)r;
    return EXIT_SUCCESS;
}

int eyestar_d2_ack(void)
{
    uint8_t ack[3] = {0x47, 0x55, 0x06};
    return eyestar_d2_write_data(ack, sizeof(ack));
}

int eyestar_d2_nack(void)
{
    uint8_t nack[3] = {0x47, 0x55, 0x0F};
    return eyestar_d2_write_data(nack, sizeof(nack));
}

int eyestar_d2_preserve_download(void)
{
    return eyestar_d2_nack();
}

int eyestar_d2_delete_download(void)
{
    return eyestar_d2_ack();
}

