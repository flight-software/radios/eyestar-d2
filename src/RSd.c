/**
 * \file RSd.c
 * \brief GlobalStar daemon for SASSI mission
 *
 * As well as being a radio daemon, we also track our coordinates, which 
 * are given to us for free for connecting to the network. We can't
 * directly compute altitude, but we may be able to derive it by
 * assuming we are in a non-decaying orbit and solving for altitude
 * from our speed (with some filtering). This *probably* isn't too
 * reliable, but we don't need a lot of graunularity for this
 *
 * GPS data and output files are synced to disk, and automatically loaded
 * into the DLL structure (if they exist) on boot, so we are pretty resilient
 * against reboots (assuming we aren't turned off while writing, but worst
 * case scenario we lose a few files)
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include "librpc.h"
#include "libdaemon.h"
#include "powerboard.h"
#include "mission_ops.h"
#include "eyestar_d2.h"

static int rsd_init(char *path){
    if(eyestar_d2_init(path)) {
        P_ERR("Failed to init path: %s", path);
        return EXIT_FAILURE;
    }

    if(mission_ops_init()) {
        P_ERR_STR("Failed to init mission ops");
        return EXIT_FAILURE;
    }

    if(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_EN, HOTSWAP_J11, 5*1000*1000)) {
        P_ERR_STR("Can't enable EyeStar-D2 hotspot");
        return EXIT_FAILURE;
    }
    P_INFO_STR("Waiting for EyeStar-D2 to wake up (for 60 seconds)");
    usleep(60*1000*1000);
    return EXIT_SUCCESS;
}

static int rsd_close(void) {
    return eyestar_d2_close();
}

int main(int argc, char **argv){
    char* serial = argc >= 2 ? argv[1] : RSD_SERIAL;
    daemonize();
    if(rsd_init(serial)) {
        P_ERR("Failed to initialize with serial: %s", serial);
        return EXIT_FAILURE;
    }
    P_INFO_STR("EyeStar-D2 is entering main loop");
    while(1){
        eyestar_d2_loop();
        mission_ops_loop();
        sleep(5);
    }
    return 0;
}
