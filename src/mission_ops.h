#ifndef MISSION_OPS_H
#define MISSION_OPS_H

#include "monitor.h"

#define RSD_MISSION_RPC_SOCKET (50301)

#define RSD_CONFIG_FILE HMD_LOG_DIR"/eyestar_d2-config"
#define RSD_SHELL_FOLDER "/var/cdh/scripts"

typedef struct {
    uint64_t phase;
    hmd_sassi_beacon_packed_fs beacon_data;
} mission_ops_t;

typedef struct {
    mission_ops_t mission_ops;
    uint8_t begin_nominal_hold_cmd_count; // Used for transitions in both directions
    time_t hold_timer;
    time_t HOLD_PHASE_WAIT_TIME_SEC;
    time_t timeout_timer;
    time_t CONNECTION_TIMEOUT_SEC;
} RSD_MISSION_OPS;

/**
 * \brief Protocol and Mission Ops
 *
 * Since the only mission we have thast uses the GlobalStar radio
 * is SASSI, all mission ops for the mission are baked into the
 * radio software directly
 *
 * This is pretty much a direct wrapper around all the GlobalStar
 * commands, and this also contains the RPC server, since
 * all commands relevant to the GlobalStar pertain to the mission
 * operations, as everything else is self contained
 *
 * Here are some technical details about stuff
 *
 * We don't use SMS messages, since our mission does the majority of
 * what it needs to do with downlink, and the 34 byte size limit is
 * pretty low for some of the conops we would be doing
 *
 * The first byte is an opcode. In the implementation, the op code
 * indexes a function pointer which receives the in_data + 2 bytes.
 * The second byte is a UUID. This UUID is used to identify the
 * opcode in the response message, to ensure that responses are
 * associated with the appropriate command.
 *
 * The return file will consist of the opcode, the UUID,
 * the status code (1 == success, 0 == failure), and
 * then the variable length response.
 */

int mission_ops_init(void);
int mission_ops_loop(void);
int mission_ops_close(void);
static int read_file_contents(const char* filename, uint8_t** buffer, size_t* len);
static int get_newest_data(const char* daemon_name, char*** filenames, size_t* num_files);
static int subsys_tx(const char* daemon_name, uint8_t* param);
static int mission_ops_rpc_set_beacon_data(uint8_t *in, size_t in_size,
                                           uint8_t *out, size_t *out_size, void *param);
static int mission_ops_rpc_query_tx_status(uint8_t *in, size_t in_size,
                                           uint8_t *out, size_t *out_size, void *param);
static int mission_ops_control_send_recv_udp(uint8_t *in, size_t in_size,
                                             uint8_t *out, size_t *out_size, void* param);
static int mission_ops_control_begin_nominal(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_revert_hold(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_progress_stage(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param);
static int mission_ops_control_downlink_file(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param);
static int mission_ops_control_downlink_file_pred(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param);
static int mission_ops_control_uplink_file(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_raw_shell(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param);
static int mission_ops_control_science_tx(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_process_queue(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_tx_count(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int mission_ops_control_delete_tx_queue(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param);
static int save_config(RSD_MISSION_OPS* config);
//static int payload_tx_wrapper(const char* filename, uint8_t* data, size_t len);
static int payload_rx_wrapper(uint8_t* data, size_t* len);
static int get_beacon_and_tx(uint8_t* param);
// 0 if not connected, 1 if connected
static int check_if_connected(uint8_t* connected);

#endif
