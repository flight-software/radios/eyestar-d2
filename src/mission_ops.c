#include <libgen.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "librpc.h"
#include "liblog.h"
#include "libdebug.h"
#include "libadcs.h"
#include "purdue.h"
#include "monitor.h"
#include "monitor_ps.h"
#include "powerboard.h"
#include "eyestar_d2.h"
#include "mission_ops.h"

static uint8_t radio_in[65536];
static size_t radio_in_size;

static uint8_t radio_out[65536];
static size_t radio_out_size;

static time_t last_time_rx_check = 0;

static RSD_MISSION_OPS mission_ops_config = {
#ifdef NDEBUG
    .HOLD_PHASE_WAIT_TIME_SEC = 12 * 60 * 60,
#else
    .HOLD_PHASE_WAIT_TIME_SEC = 2 * 60,
#endif
    .CONNECTION_TIMEOUT_SEC = 3 * 60
};
static rpc_server_fs rpc_server;

// All phrasing is from the persoective of the ground (ground control)
// So send uploads, recv downloads, etc
static int(*mission_ops_controls[])(uint8_t *, size_t, uint8_t *, size_t *, void* param) = {
    mission_ops_control_send_recv_udp, // Integration time can be set with
    mission_ops_control_begin_nominal,
    mission_ops_control_revert_hold,
    mission_ops_control_raw_shell,
    mission_ops_control_science_tx,
    mission_ops_control_progress_stage,
    mission_ops_control_downlink_file,
    mission_ops_control_uplink_file,
    mission_ops_control_tx_count
};

// 0 if not connected, 1 if connected
static int check_if_connected(uint8_t* connected)
{
    // for the radio side, 1 is not connected, 0 is connected
    eyestar_d2_soh_fs health;
    if(eyestar_d2_read_soh(&health)) {
        P_ERR_STR("Failed to read soh");
        return EXIT_FAILURE;
    }
    *connected = health.connection_status == 1 ? 0 : 1;
    return EXIT_SUCCESS;
}

static int get_newest_data(const char* daemon_name, char*** filenames, size_t* num_files) {
    if(!filenames || !num_files) return EXIT_FAILURE;
    *filenames = NULL;
    typedef struct {
        char daemon[5];
        uint64_t type, time_;
        char* filename;
    } log_file;
    DIR* log_dir = opendir(LOG_PATH);
    if(!log_dir) {
        P_ERR("Cannot open log dir: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    struct dirent* ep;
    size_t file_count = 0;
    log_file* lf_arr = NULL;
    while((ep = readdir(log_dir)) != NULL) {
        log_file lf = {0};
        int matched = sscanf(ep->d_name, "%c%c%c%c_%"PRIu64"_%"PRIu64, lf.daemon, lf.daemon + 1, lf.daemon + 2, lf.daemon + 3, &lf.type, &lf.time_);
        lf.daemon[4] = '\0';
        if(matched != 6) continue; // "." ".." "etc..."
        if(strcmp(lf.daemon, daemon_name) == 0) {
            int seen_type = 0;
            for(size_t i = 0; i < file_count; i++) {
                if(lf.type == lf_arr[i].type) {
                    seen_type = 1;
                    if(lf.time_ > lf_arr[i].time_) {
                        free(lf_arr[i].filename);
                        lf.filename = strdup(ep->d_name);
                        lf_arr[i] = lf;
                        break;
                    }
                }
            }
            if(!seen_type) { // new "type"
                lf.filename = strdup(ep->d_name);
                file_count += 1;
                lf_arr = realloc(lf_arr, file_count * sizeof(*lf_arr));
                lf_arr[file_count - 1] = lf;
            }
        }
    }
    closedir(log_dir); log_dir = NULL;
    *num_files = file_count;
    *filenames = realloc(*filenames, file_count * sizeof(char*));
    for(size_t i = 0; i < file_count; i++) {
        (*filenames)[i] = calloc(1, sizeof(LOG_PATH"/") + strlen(lf_arr[i].filename));
        strcpy((*filenames)[i], LOG_PATH"/");
        strcat((*filenames)[i], lf_arr[i].filename);
        free(lf_arr[i].filename); lf_arr[i].filename = NULL;
        // We strip off the basename later on anyways but I dont want to break it
        // Dillon Hammond 10/09/2018
    }
    free(lf_arr); lf_arr = NULL;
    return EXIT_SUCCESS;
}

static int subsys_tx(const char* daemon_name, uint8_t* param)
{
    (void)param; // TODO: Add back it command responses?
    char** filenames = NULL;
    size_t num_files = 0;
    if(get_newest_data(daemon_name, &filenames, &num_files)) {
        P_ERR("Failed to get newest data for subsys %s", daemon_name);
        free(filenames); filenames = NULL;
        return EXIT_FAILURE;
    }
    P_DEBUG("For %s uploading:", daemon_name);
    for(size_t i = 0; i < num_files; i++) {
        size_t filename_len = strlen(filenames[i]);
        char* base = basename(filenames[i]);
        size_t base_len = strlen(base);
        char* buffer = calloc(filename_len + base_len + 2, sizeof(char));
        memcpy(buffer, filenames[i], filename_len);
        memcpy(buffer + filename_len + 1, base, base_len);
        if(eyestar_d2_payload_tx_rpc((uint8_t*)buffer, filename_len + base_len + 2, NULL, NULL, NULL)) {
            P_ERR("Couldn't TX filename (%s), basename (%s)", filenames[i], base);
        } else {
            P_DEBUG("TXed filename (%s), basename (%s)", filenames[i], base);
        }
        free(buffer); buffer = NULL;
        free(filenames[i]); filenames[i] = NULL;
    }
    free(filenames); filenames = NULL;
    return EXIT_SUCCESS;
}

static int mission_ops_rpc_set_beacon_data(uint8_t *in, size_t in_size,
                                           uint8_t *out, size_t *out_size, void *param)
{
    (void)out; (void)param;
    *out_size = 0;
    if(in_size < sizeof(mission_ops_config.mission_ops.beacon_data)) return EXIT_FAILURE;

    memcpy(&mission_ops_config.mission_ops.beacon_data, in, sizeof(mission_ops_config.mission_ops.beacon_data));
    return EXIT_SUCCESS;
}

static int mission_ops_rpc_query_tx_status(uint8_t *in, size_t in_size,
                                           uint8_t *out, size_t *out_size, void *param)
{
    (void)in; (void)in_size; (void)param;
    uint64_t last_time = eyestar_d2_get_last_tx_s();
    if(last_time > ((uint64_t)time(NULL) - 180UL)) {
        *out_size = sizeof(RADIO_TX_DONE);
        *out = RADIO_TX_DONE;
    } else {
        *out_size = sizeof(RADIO_NOT_DONE);
        *out = RADIO_NOT_DONE;
    }
    return EXIT_SUCCESS;
}

static int mission_ops_control_process_queue(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)in; (void)in_size; (void)param; (void)out;
    uint32_t count = 0;
    if(eyestar_d2_get_rx_count(&count) && count > 0) {
        radio_in_size = sizeof(radio_in);
        if(payload_rx_wrapper(radio_in, &radio_in_size)) {
            if(out_size) *out_size = 0;
            return EXIT_FAILURE;
        } else {
            if(radio_in_size == 0) return EXIT_FAILURE;
            
            if(radio_in[0] >= sizeof(mission_ops_controls)/sizeof(mission_ops_controls[0])){
                P_ERR_STR("OP code does not match to a callback");
                // TODO: Maybe continue?
                return EXIT_FAILURE;
            }

            P_DEBUG("Received an OP code of %d and a uuid of %d", (int)radio_in[0], (int)radio_in[1]);
            
            mission_ops_controls[radio_in[0]](radio_in + 2, radio_in_size, radio_out, &radio_out_size, radio_in);
        }
    }
    return EXIT_FAILURE;
}

static int mission_ops_control_delete_tx_queue(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)in; (void)in_size; (void)param; (void)out;
    if(out_size) *out_size = 0;
    return eyestar_d2_delete_tx_queue();
}

static int recover_config(void)
{
    FILE* fp = fopen(RSD_CONFIG_FILE, "r");
    if(!fp) goto recover_config_backup;
    if(fread(&mission_ops_config, sizeof(mission_ops_config), 1, fp) != 1) {
        P_ERR_STR("Failed to read mission ops config even though file exists");
        fclose(fp);
        memset(&mission_ops_config, 0, sizeof(mission_ops_config));
        goto recover_config_backup;
    }
    fclose(fp);
    return EXIT_SUCCESS;
recover_config_backup:
    fp = fopen(RSD_CONFIG_FILE".bak", "r");
    if(!fp) return EXIT_FAILURE;
    if(fread(&mission_ops_config, sizeof(mission_ops_config), 1, fp) != 1) {
        P_ERR_STR("Failed to read mission ops config even though file exists");
        fclose(fp);
        memset(&mission_ops_config, 0, sizeof(mission_ops_config));
        return EXIT_FAILURE;
    }
    fclose(fp);
    return EXIT_SUCCESS;
}

int mission_ops_init(){
    recover_config();

    if(rpc_server_init(&rpc_server, RSD_MISSION_RPC_SOCKET, RPC_SERVER_MT) == EXIT_FAILURE){
        P_ERR_STR("Cannot create RPC server");
        return EXIT_FAILURE;
    }
    rpc_server_add_elem(&rpc_server, RSD_OP_BEGIN_NOMINAL, 
            rpc_rpc_get_uint64, &(mission_ops_config.mission_ops.phase), NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_QUERY_TX_STATUS,
            mission_ops_rpc_query_tx_status, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_GET_TX_COUNT,
            mission_ops_control_tx_count, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_SCIENCE_TX,
            mission_ops_control_science_tx, NULL, NULL);
    rpc_server_add_elem(&rpc_server, RSD_OP_PROCESS_QUEUE,
            mission_ops_control_process_queue, NULL, NULL);
    return EXIT_SUCCESS;
}

static int mission_ops_control_send_recv_udp(uint8_t *in, size_t in_size,
                                             uint8_t *out, size_t *out_size, void* param)
{
    (void)param;
    if(in_size < 2 * sizeof(uint16_t)) return EXIT_FAILURE;
    const uint16_t port = *((uint16_t*)in);
    in += sizeof(uint16_t);
    const uint16_t opcode = *(uint16_t*)in;
    in += sizeof(uint16_t);
    const size_t data_size = in_size - 2*sizeof(uint16_t);

    if(rpc_send_recv(port, opcode, in, data_size, out, out_size, 5*1000*1000)) {
        P_ERR_STR("Failed to do udp command")
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int mission_ops_control_begin_nominal(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)param;
    if(out_size) *out_size = sizeof(uint64_t); // Always true
    if(mission_ops_config.mission_ops.phase == RADIO_BEGIN_NOMINAL) {
        if(out) *((uint64_t*)out) = RADIO_BEGIN_NOMINAL;
        return EXIT_SUCCESS;
    }
    // We use sizeof the string to enforce the NULL termination in the file
    if(in_size < sizeof(BEGIN_NOMINAL_PHRASE)) return EXIT_FAILURE;
    char* file_data = (char*)in;
    if(strncmp(file_data, BEGIN_NOMINAL_PHRASE, sizeof(BEGIN_NOMINAL_PHRASE))) {
        mission_ops_config.begin_nominal_hold_cmd_count++;
        if(mission_ops_config.begin_nominal_hold_cmd_count >= 2) {
            mission_ops_config.mission_ops.phase = RADIO_BEGIN_NOMINAL;
            mission_ops_config.begin_nominal_hold_cmd_count = 0;
            if(out) *((uint64_t*)out) = RADIO_BEGIN_NOMINAL;
        } else {
            if(out) *((uint64_t*)out) = RADIO_STAY_HOLD;
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

static int mission_ops_control_revert_hold(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)in; (void)in_size; (void)out; (void)param;
    if(out_size) *out_size = 0; // Always true
    if(mission_ops_config.mission_ops.phase == RADIO_STAY_HOLD) {
        return EXIT_SUCCESS;
    }
    // We use sizeof the string to enforce the NULL termination in the file
    if(in_size < sizeof(REVERT_TO_HOLD_PHRASE)) return EXIT_FAILURE;
    char* file_data = (char*)in;
    if(strncmp(file_data, REVERT_TO_HOLD_PHRASE, sizeof(REVERT_TO_HOLD_PHRASE))) {
        mission_ops_config.begin_nominal_hold_cmd_count++;
        if(mission_ops_config.begin_nominal_hold_cmd_count >= 2) {
            mission_ops_config.mission_ops.phase = RADIO_STAY_HOLD;
            mission_ops_config.begin_nominal_hold_cmd_count = 0;
            int i = 0;
            while(i < 5 && rpc_trigger_action(PSD_THREAD_RPC_SOCKET, HMD_SASSI_REVERT_TO_HOLD, 10000000) != EXIT_SUCCESS) {
                sleep(1);
                i += 1;
                P_ERR("Tried to revert health monitor to HOLD %d/5", i);
            }
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

static int mission_ops_control_progress_stage(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param){
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    return EXIT_SUCCESS;
}

static int mission_ops_control_downlink_file(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param){
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    return EXIT_SUCCESS;
}

// Maps the predictable filenames to actual files on the satellite
// I'm not directly modifying the names, but just run under the assumption
// that no files are increasing

static int mission_ops_control_downlink_file_pred(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param)
{
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    DIR *dp = opendir(LOG_PATH);
    if(dp == NULL){
        P_ERR("Cannot open log dir: %d", errno);
        return EXIT_FAILURE;
    }

    const char daemon_name[5] = {(char)in[0], (char)in[1], (char)in[2], (char)in[3], '\0'};
    in += 4;
    uint64_t log_type = *in;
    in += 1;
    uint64_t offset = *((uint32_t*)in);
    uint64_t downlink_file[5000] = {0}; // I hope we dont have more than this
    
    struct dirent *ep;
    size_t log_len = 0;
    while((ep = readdir(dp))) {
        const char found_daemon[5] = {0};
        uint64_t type, time_;
        sscanf(ep->d_name, "%c%c%c%c_%"PRIu64"_%"PRIu64, found_daemon, found_daemon + 1, found_daemon + 2, found_daemon + 3, &type, &time_);
        if(strcmp(daemon_name, found_daemon) == 0 && type == log_type){
            downlink_file[log_len] = time_;
            log_len++;
        }
    }
    closedir(dp);

    uint8_t move = 1;
    while(move){
        move = 0;
        for(size_t i = 0;i < log_len - 1;i++){
            if(downlink_file[i] > downlink_file[i+1]){
                downlink_file[i] ^= downlink_file[i+1];
                downlink_file[i+1] ^= downlink_file[i];
                downlink_file[i] ^= downlink_file[i+1];
                move = 1;
                break;
            }
        }
    }
    
    char filename[256];
    memset(filename, 0, 256); 
    sprintf(filename, "/var/log/cdh/logs/%c%c%c%c_%"PRIu64"_%"PRIu64, daemon_name[0], daemon_name[1], daemon_name[2], daemon_name[3], log_type, downlink_file[offset]);

    FILE *file = fopen(filename, "r");
    if(file == NULL){
        P_ERR_STR("OH FUG");
        return EXIT_FAILURE;
    }
    
    *out_size = fread(out, 1, *out_size, file);
    fclose(file);
    return EXIT_SUCCESS;
}

static int mission_ops_control_uplink_file(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    char* file_name = (char*)in;
    size_t file_name_len = 0;
    {
        size_t i;
        for(i = 0; (i < in_size) || (file_name_len >= 255); i++) {
            if(file_name[i] != '\0') {
                file_name_len += 1;
            } else {
                break;
            }
        }
        if(i == in_size || file_name_len >= 255) { // No null terminator or name too long
            return EXIT_FAILURE;
        }
    } // file_name was null terminated
    char* file_contents = file_name + (file_name_len + 1);
    size_t file_contents_len = in_size - (file_name_len + 1); // +1 for NULL byte
    
    size_t full_path_len = sizeof(RSD_SHELL_FOLDER); // Folder with NULL terminator, no ending slash
    full_path_len += 1; // slash
    full_path_len += file_name_len; // file name
    char* file_full_path = calloc(full_path_len, sizeof(char));
    snprintf(file_full_path, full_path_len, RSD_SHELL_FOLDER"/%s", file_name);
    
    int err = EXIT_SUCCESS;
    FILE* fp = fopen(file_full_path, "w"); // overwrite
    if(fp == NULL) {
        P_ERR("mission ops failed to open file for uplink: %d (%s)", errno, strerror(errno));
        err = EXIT_FAILURE;
    } else if(fwrite(file_contents, sizeof(char), file_contents_len, fp) != file_contents_len) {
        P_ERR_STR("mission ops failed to write all data");
        err = EXIT_FAILURE;
    }
    if(fp) fclose(fp);
    fp = NULL;
    if(err == EXIT_FAILURE) {
        free(file_full_path); file_full_path = NULL;
        return EXIT_FAILURE;
    }
    free(file_full_path); file_full_path = NULL;
    return EXIT_SUCCESS;
}

static int mission_ops_control_raw_shell(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void* param)
{
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    char* cmd = (char*)in;
    size_t cmd_len = 0;
    {
        size_t i;
        for(i = 0; i < in_size; i++) {
            if(cmd[i] != '\0') {
                cmd_len += 1;
            } else {
                break;
            }
        }
        if(i == in_size) {
            return EXIT_FAILURE;
        }
    } // cmd was null terminated
    if(out_size) *out_size = 0;
    if(system(cmd)) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int mission_ops_control_tx_count(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)in; (void)in_size; (void)out; (void)out_size; (void)param;
    if(!out || *out_size < sizeof(uint32_t)) return EXIT_FAILURE;
    *out_size = sizeof(uint32_t);
    if(eyestar_d2_get_tx_count((uint32_t*)out)) {
        P_ERR_STR("Failed to get tx count");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int mission_ops_control_science_tx(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    (void)out; (void)param;
    if(out_size) *out_size = 0;
    if(in && in_size > 0) {
        P_DEBUG_STR("DOING THE LOOP THING");
        for(size_t i = 0; i < in_size; i++) {
            int err;
            switch(in[i]) {
                case SUBSYS_FLAME:;
                    err = subsys_tx(PSD_BIN, (uint8_t*)param);
                    if(err) {
                        P_ERR_STR("Failed to TX FLAME data");
                        continue;
                    }
                    // Don't need to rotate logs
                break;
                case SUBSYS_ADCS:;
                    err = subsys_tx(ACD_BIN, (uint8_t*)param);
                    if(err) {
                        P_ERR_STR("Failed to TX ADCS data");
                        continue;
                    }
                    rpc_trigger_action(ACD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
                break;
                case SUBSYS_PSP:;
                    err = subsys_tx(PPD_BIN, (uint8_t*)param);
                    if(err) {
                        P_ERR_STR("Failed to TX PSP data");
                        continue;
                    }
                    rpc_trigger_action(PSD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
                break;
                case SUBSYS_CADH:;
                    err = subsys_tx(HMD_BIN, (uint8_t*)param); // No split log needed
                    if(err) {
                        P_ERR_STR("Failed to TX BEACON data");
                        continue;
                    }
                break;
                default:;
                    continue;
            }
        }
        rpc_trigger_action(PBD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
        return EXIT_SUCCESS;
    }
    // 1. Get HMd beacon
    P_DEBUG_STR("GOING TO TX BEACON");
    if(subsys_tx(HMD_BIN, (uint8_t*)param)) {
        P_ERR_STR("Failed to TX BEACON data");
    }
    // 2. Get spectra
    P_DEBUG_STR("GOING TO TX PSD");
    if(subsys_tx(PSD_BIN, (uint8_t*)param)) {
        P_ERR_STR("Failed to TX FLAME data");
    } // Don't need to rotate logs cause daemon killed often
    // 3. Get adcs
    P_DEBUG_STR("GOING TO TX ACD");
    if(subsys_tx(ACD_BIN, (uint8_t*)param)) {
        P_ERR_STR("Failed to TX ADCS data");
    } else {
        rpc_trigger_action(ACD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
    }
    // 4. Get purdue
    P_DEBUG_STR("GOING TO TX PPD");
    if(subsys_tx(PPD_BIN, (uint8_t*)param)) {
        P_ERR_STR("Failed to TX PSP data");
    } else {
        rpc_trigger_action(PSD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
    }
    rpc_trigger_action(PBD_RPC_SOCKET, RPC_OP_SPLIT_LOG, 10*1000000);
    return EXIT_SUCCESS;
}

static int save_config(RSD_MISSION_OPS* config) {
    rename(RSD_CONFIG_FILE, RSD_CONFIG_FILE".bak"); // Save old as backup
    FILE* fp = fopen(RSD_CONFIG_FILE, "w");
    if(fp == NULL) {
        P_ERR("mission_ops failed to open config file: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fwrite(config, sizeof(*config), 1, fp) != 1) {
        P_ERR_STR("mission_ops failed to write config to file");
    }
    if(fclose(fp)) {
        P_ERR("mission_ops failed to close config file: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int payload_rx_wrapper(uint8_t* data, size_t* len)
{
    return eyestar_d2_payload_rx(data, len);
}

int mission_ops_loop(){
    P_INFO_STR("Running mission ops");
    if(mission_ops_config.mission_ops.phase == RADIO_STAY_HOLD && mission_ops_config.hold_timer == 0) {
        mission_ops_config.hold_timer = time(NULL);
    }
    if(mission_ops_config.mission_ops.phase == RADIO_STAY_HOLD && time(NULL) > (mission_ops_config.hold_timer + mission_ops_config.HOLD_PHASE_WAIT_TIME_SEC)) {
        P_INFO("%d seconds has passed. Moving from hold to nominal.", (int)(time(NULL) - mission_ops_config.hold_timer));
        mission_ops_config.mission_ops.phase = RADIO_BEGIN_NOMINAL;
        mission_ops_config.hold_timer = 0;
    }

    if(mission_ops_config.timeout_timer == 0) {
        mission_ops_config.timeout_timer = time(NULL);
    }

    save_config(&mission_ops_config);

    {
        time_t n = time(NULL);
        if(n - last_time_rx_check < 60) return EXIT_SUCCESS; // 60 second loop
        last_time_rx_check = n;
    }

    radio_in_size = sizeof(radio_in);

    P_DEBUG_STR("STARTING PAYLOAD_RX_WRAPPER");
    uint32_t count = 0;
    if(eyestar_d2_get_rx_count(&count) == EXIT_SUCCESS && count > 0) {
        if(payload_rx_wrapper(radio_in, &radio_in_size)) {
            return EXIT_FAILURE;
        }
    } else {
        return EXIT_SUCCESS;
    }

    if(radio_in_size == 0){
        return EXIT_SUCCESS;
    }
    
    P_DEBUG("Received an OP code of %d and a uuid of %d", (int)radio_in[0], (int)radio_in[1]);
    
    if(radio_in[0] >= sizeof(mission_ops_controls)/sizeof(mission_ops_controls[0])){
        P_ERR_STR("OP code does not match to a callback");
        return EXIT_FAILURE;
    }

    if(mission_ops_controls[radio_in[0]](radio_in + 2, radio_in_size, radio_out, &radio_out_size, radio_in)) {
        P_ERR("Failed running op code: %d", (int)radio_in[0]);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int mission_ops_close(){
    rpc_server_close(&rpc_server);
    return EXIT_SUCCESS;
}

