#ifndef GLOBALSTAR_H
#define GLOBALSTAR_H
/**
 * \brief GlobalStar Radio Driver
 *
 * Technically EyeStar-D2
 *
 * This radio is special, since it can give us GPS coordinates
 * from the last connection (so long as we have things to
 * downlink)
 *
 * SASSI payload could also benefit from knowing an approximate
 * altitude so we can continuously adjust spectrometer parameters
 */

#include <stdint.h>
#include <pthread.h>

#include "libdebug.h"

#define RSD_SERIAL "/dev/ttyO2"

#define RSD_BIN "rs-d"

#define RSD_RPC_SOCKET (50300)

#define RSD_OP_CHECK_ALIVE     (0x01)
#define RSD_OP_GET_RX_COUNT    (0x02)
#define RSD_OP_GET_TX_COUNT    (0x03)
#define RSD_OP_PAYLOAD_TX      (0x04)
#define RSD_OP_PAYLOAD_RX      (0x05)
#define RSD_OP_CHECK_SOH       (0x06)
#define RSD_OP_CLEAR_TX_QUEUE  (0x07)
#define RSD_OP_GET_LAST_TX_S   (0x08)
#define RSD_OP_BEGIN_NOMINAL   (0x09)
#define RSD_OP_QUERY_TX_STATUS (0xA0)
#define RSD_OP_SCIENCE_TX      (0xA1)
#define RSD_OP_PROCESS_QUEUE   (0xA2)

#define RADIO_STAY_HOLD       UINT64_C(0x0)
#define RADIO_BEGIN_NOMINAL   UINT64_C(0x1)
#define RADIO_NOT_DONE        UINT64_C(0x2)
#define RADIO_TX_DONE         UINT64_C(0x3)
#define RADIO_HOLD_PHASE_DONE UINT64_C(0x4)

#define BEGIN_NOMINAL_PHRASE ("BEGIN_NOMINAL")
#define REVERT_TO_HOLD_PHRASE ("ERR_REVERT_TO_HOLD")

#define RADIO_TX_WAIT_TIME_USEC (60*1000000) // 60 seconds

#define RADIO_TX_WAIT_TIME_USEC (60*1000000) // 60 seconds

extern pthread_mutex_t serial_lock;

/**
 * \brief GlobalStar GPS readings
 *
 * We can prompt for these over the air, which would be nice
 */
struct eyestar_d2_gps_fs {
    int16_t n_deg;
    int16_t n_min;
    int16_t n_sec;

    int16_t w_deg;
    int16_t w_min;
    int16_t w_sec;
    
    uint32_t time_s; // Time of collection locally
};

typedef struct eyestar_d2_gps_fs eyestar_d2_gps_fs;

/**
 * \brief State of Health
 *
 * Pretty much everything we want to know
 */

struct eyestar_d2_soh_fs {
    uint32_t epoch_reset;
    uint32_t cur_time_s;
    uint32_t last_connect_time_s;
    uint32_t last_attempt_time_s;
    uint32_t call_attempts;
    uint32_t successful_connects;
    uint32_t avg_conn_dur_s;
    uint32_t std_dev_conn_dur_s;
    uint8_t rssi;
    uint8_t connection_status;
    uint8_t gateway; // proprietary boogeyman
};

typedef struct eyestar_d2_soh_fs eyestar_d2_soh_fs;

int eyestar_d2_init(char *path);
int eyestar_d2_loop(void);
int eyestar_d2_close(void);
int eyestar_d2_check_alive(void);
int eyestar_d2_get_rx_count(uint32_t* count);
int eyestar_d2_get_tx_count(uint32_t* count);
int eyestar_d2_payload_tx(const char *filename, const uint8_t *data, size_t len);
uint64_t eyestar_d2_get_last_tx_s(void);
int eyestar_d2_payload_rx(uint8_t *data, size_t *len);
int eyestar_d2_read_soh(eyestar_d2_soh_fs *soh);
int eyestar_d2_ack(void);
int eyestar_d2_nack(void);
int eyestar_d2_preserve_download(void);
int eyestar_d2_delete_download(void);
int eyestar_d2_delete_tx_queue(void);
int eyestar_d2_payload_tx_rpc(uint8_t* in, size_t in_len, uint8_t* out, size_t* out_len, void* param);

#endif
